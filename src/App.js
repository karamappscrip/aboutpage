import logo from "./logo.svg";
import "./App.css";
import AboutUs from "./components/AboutUs";

function App() {
  return (
    <div>
      <AboutUs />
    </div>
  );
}

export default App;
