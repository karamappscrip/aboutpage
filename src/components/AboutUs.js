import React from "react";
import main from "./svg/main.svg";
import grid1 from "./svg/grid1.svg";
import grid3 from "./svg/grid3.svg";
import grid2 from "./svg/grid2.svg";
import one from "./svg/one.png";
import two from "./svg/two.png";
import facebook from "./svg/facebook.png";
import twitter from "./svg/twitter.png";
import instagram from "./svg/gram.png";
import apple from "./svg/111.png";
import email from "./svg/222.png";
import three from "./svg/three.png";
import thunder from "./svg/thunder.png";
import global from "./svg/global.png";
import check from "./svg/check.png";
import live from "./svg/live.png";

import "../Css/About.css";

const AboutUs = () => {
  return (
    <React.Fragment>
      <section className="container   background_color  overflow">
        <div className="container   background_color">
          <div className="row   colm_reverse">
            <div className="col-md-6 my-5 order-md-1 order-sm-2 ">
              <h1 className="fontsize ">
                Stay upto date with your favorite stars and subscribe to get
                access to their most exclusive content
              </h1>
            </div>
            <div className="col-md-6 my_col order-md-2  order-sm-1 ">
              <div className="imgcontainer">
                <img className="mainImg" src={main} alt="error" />
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className="container overflow ">
        <div>
          <h3 className="work_text"> How Fanzly Works </h3>
        </div>
        <div className="container">
          <div className="row   colm_reverse">
            <div className="col-md-6 flex_center  order-md-1 order-sm-2">
              <div className="dflex">
                <h6 className="heading">
                  <img className="border_radius" src={one} alt="error" />
                  1:1 MESSAGING
                </h6>
                <h1 className="font-size">
                  Find the right celeb for any occasion
                </h1>
                <p className="psize">
                  Birthdays, milestones, or even a well-deserved roast, the
                  perfect celebrity is only a search away. Find yours and
                  request them
                </p>
              </div>
            </div>
            <div className="col-md-6 my-5 order-md-2  order-sm-1">
              <img className="fit_in_col" src={grid1} alt="error" />
            </div>
          </div>

          <div className="row">
            <div className="col-md-6  order-md-1  order-sm-2">
              <img className="fit_in_col" src={grid3} alt="error" />
            </div>
            <div className="col-md-6 flex_center  order-md-2 order-sm-1">
              <div className="dflex">
                <h6 className="heading">
                  {" "}
                  <img className="border_radius" src={two} alt="error" />
                  EXCLUSIVE CONTENT
                </h6>
                <h1 className="font-size">
                  Get your personalized video message
                </h1>
                <p className="psize">
                  Include all the important details in your request form. After
                  it’s submitted, stars have up to 7 days to complete it. Choose
                  our 24hr delivery option if you need it sooner.
                </p>
              </div>
            </div>
          </div>

          <div className="row  colm_reverse">
            <div className="col-md-6 flex_center  order-md-1 order-sm-2">
              <div className="dflex">
                <h6 className="heading">
                  {" "}
                  <img className="border_radius" src={three} alt="error" />
                  SEARCH FOR A STAR
                </h6>
                <h1 className="font-size">
                  Be the first to know when they start a live event
                </h1>
                <p className="psize">
                  Magical moments deserve to be shared. Whether you’re giving
                  one or receiving a personalized video, we want to see your
                  reaction. Bonus points if you tag us.
                </p>
              </div>
            </div>
            <div className="col-md-6 my-5 order-md-2  order-sm-1">
              <img className="fit_in_col" src={grid2} alt="error" />
            </div>
          </div>

          <div className="row   margin_none">
            <div className="col-md-6 my-5 order-md-1  order-sm-2">
              <img className="fit_in_col" src={grid3} alt="error" />
            </div>
            <div className="col-md-6 flex_center  order-md-2 order-sm-1">
              <div className="dflex">
                <h6 className="heading ">FAN CLUBS</h6>
                <h1 className="font-size">
                  Get exclusive access with Fan Clubs
                </h1>
                <p className="psize">
                  Stay up-to-date with your favourite personalities and get
                  access to exclusive Fan Club content, promotions, and price
                  drops.
                </p>
                <div className="list_style">
                  <li>
                    {" "}
                    <img className="border_radius1" src={check} alt="error" />
                    Insider access to special promotions
                  </li>
                  <li>
                    {" "}
                    <img className="border_radius1" src={thunder} alt="error" />
                    Exclusive updates directly from celebrities{" "}
                  </li>
                  <li>
                    {" "}
                    <img className="border_radius1" src={live} alt="error" />
                    Be the first to know when they're hosting a live event
                  </li>
                  <li>
                    <img className="border_radius1" src={global} alt="error" />
                    Share your love for your favourite celebrity with other fans
                  </li>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer className="container overflow marginfooter hide">
        <div className="footerMainDiv">
          <div className="col-6">
            <p>Fanzly Logo</p>
            <br />
            <p className="padding">
              Fanzly connects fans directly with their favourite stars and
              culture icons via personalized video messages , live 1:1 calls ,
              1:1 messaging and exclusive content that can be subscribed to
            </p>
          </div>
          <div className="col-2 dflex_footer stylenone">
            <li>FAQ</li>
            <li>Accessibility</li>
            <li>Privacy Policy</li>
            <li>Terms and Conditions</li>
          </div>
          <div className="col-4 dflex_icon">
            <div className="img_flex">
              <img className="border_icons" src={facebook} alt="error" />
              <img className="border_icons" src={instagram} alt="error" />
              <img className="border_icons" src={apple} alt="error" />
              <img className="border_icons" src={email} alt="error" />
              <img className="border_icons" src={twitter} alt="error" />
            </div>
            <p>© 2018-2022, Baron App, Inc. dba Onlyvids</p>
          </div>
        </div>
      </footer>
    </React.Fragment>
  );
};

export default AboutUs;
